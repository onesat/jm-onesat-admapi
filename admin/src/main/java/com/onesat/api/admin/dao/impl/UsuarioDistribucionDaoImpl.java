package com.onesat.api.admin.dao.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.onesat.api.admin.UsuarioDistribucionDTO;
import com.onesat.api.admin.UsuarioDistribucionDTO.Busqueda;
import com.onesat.api.admin.basico.Usuario;
import com.onesat.api.admin.dao.UsuarioDistribucionDao;
import com.onesat.api.admin.enumerados.distribucion.EstadoDistribucion;
import com.onesat.api.admin.enumerados.usuario.TipoUsuario;
import com.repex.librerias.criptografia.Criptografia;
import com.repex.librerias.login.entidades.EstadoUsuario;
import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseList;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;

public class UsuarioDistribucionDaoImpl implements UsuarioDistribucionDao {

	public List<UsuarioDistribucionDTO> recuperarUsarios(Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {

		List<UsuarioDistribucionDTO> resultado = new LinkedList<UsuarioDistribucionDTO>();
		
		// FUNCION
		String FUNCION = "recuperar_usuarios_distribucion";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		if(busqueda != null) {
			
			if(busqueda.getReferencia() != null && !busqueda.getReferencia().equals("")) {
				nodos.add(new SOAPEnvironment.Nodo("REFERENCIA_USUARIO", "xsd:string", busqueda.getReferencia()));				
			}
			if(busqueda.getNombre() != null && !busqueda.getNombre().equals("")) {
				nodos.add(new SOAPEnvironment.Nodo("NOMBRE_USUARIO", "xsd:string", busqueda.getNombre()));
			}
			if(busqueda.getEstado() != null) {
				nodos.add(new SOAPEnvironment.Nodo("ESTADO_USUARIO", "xsd:int", busqueda.getEstado().id));
			}
			if(busqueda.getTipo() != null) {
				nodos.add(new SOAPEnvironment.Nodo("TIPO_USUARIO", "xsd:int", busqueda.getTipo().id));
			}
		}
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("busqueda", "tns:BusquedaUsuarioDistribucion", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {
			
			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {
				
				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseList response = lastResponse.getMultiData();
			for(SOAPResponseMap map : response.getDatos()) {
				
				UsuarioDistribucionDTO usuarioDistribucion = new UsuarioDistribucionDTO();
				
				Usuario usuario = new Usuario();				
				usuario.setId(Integer.parseInt(map.getAtributte("ID_USUARIO")));
				usuario.setReferencia(map.getAtributte("REFERENCIA_USUARIO"));
				usuario.setNombre(map.getAtributte("NOMBRE_USUARIO"));
				usuario.setContrasena(Criptografia.getSingleton().decrypt(map.getAtributte("CONTRASENA_USUARIO")));
				usuario.setEstado(EstadoUsuario.values()[Integer.parseInt(map.getAtributte("ESTADO_USUARIO")) - 1]);
				usuario.setTipo(TipoUsuario.values()[Integer.parseInt(map.getAtributte("TIPO_USUARIO")) - 1]);				
				usuarioDistribucion.setUsuario(usuario);
				
				try {
					
					usuarioDistribucion.setIdDistribucion(Integer.parseInt(map.getAtributte("ID_DISTRIBUCION")));
					usuarioDistribucion.setDescripcionDistribucion(map.getAtributte("DESCRIPCION_DISTRIBUCION"));
					usuarioDistribucion.setEstadoDistribucion(EstadoDistribucion.values()[Integer.parseInt(map.getAtributte("ESTADO_DISTRIBUCION")) - 1]);		
					
				} catch (Exception e) {}
				
				resultado.add(usuarioDistribucion);
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
			
		} catch (SAXException e) {
			
			e.printStackTrace();
		}
				
		return resultado;
	}
}
