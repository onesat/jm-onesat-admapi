package com.onesat.api.admin.basico;

import java.awt.Color;

import com.onesat.api.admin.enumerados.usuario.TipoUsuario;
import com.onesat.api.admin.utilidades.Colores;

public class Usuario extends com.repex.librerias.login.entidades.Usuario {
	
	private String referencia;	
	private TipoUsuario tipo;
	
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public TipoUsuario getTipo() {
		return tipo;
	}

	public void setTipo(TipoUsuario tipo) {
		this.tipo = tipo;
	}

	public Color getColorEstado() {
	
		switch (getEstado()) {
		
			case ACTIVADO:
				return Colores.COLOR_ACTIVADO;
				
			case DESATIVADO:
				return Colores.COLOR_DESACTIVADO;
		}
		
		return Colores.COLOR_ACTIVADO;
	}
	
	@Override
	public String toString() {
		
		return getId() + ":: " + getNombre();
	}
	
	public static class Busqueda extends com.repex.librerias.login.entidades.Usuario.Busqueda {

		private Integer id;
		private TipoUsuario tipo;

		public Busqueda() {
			
			super();
			
			this.id = 0;
			this.tipo = null;
		}
		
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
		
		public TipoUsuario getTipo() {
			return tipo;
		}
		
		public void setTipo(TipoUsuario tipo) {
			this.tipo = tipo;
		}
	}
}