package com.onesat.api.admin.basico;

import java.awt.Color;

public class GamaColor {

	private int id;
	
	private String descripcion;
	
	private Color cabecera;
	private Color elemento;
	private Color fondo;
	private Color titulo;
	private Color texto;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = (descripcion == null) ? "" : descripcion;
	}
	
	public Color getCabecera() {
		return cabecera;
	}
	
	public void setCabecera(Color cabecera) {
		this.cabecera = (cabecera == null) ? Color.WHITE : cabecera;
	}
	
	public Color getElemento() {
		return elemento;
	}
	
	public void setElemento(Color elemento) {
		this.elemento = (elemento == null) ? Color.WHITE : elemento;
	}
	
	public Color getFondo() {
		return fondo;
	}
	
	public void setFondo(Color fondo) {
		this.fondo = (fondo == null) ? Color.WHITE : fondo;
	}
	
	public Color getTitulo() {
		return titulo;
	}
	
	public void setTitulo(Color titulo) {
		this.titulo = (titulo == null) ? Color.BLACK : titulo;
	}
	
	public Color getTexto() {
		return texto;
	}
	
	public void setTexto(Color texto) {
		this.texto = (texto == null) ? Color.BLACK : texto;
	}
}
