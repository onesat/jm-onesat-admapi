package com.onesat.api.admin.dao;

import java.util.List;

import javax.xml.soap.SOAPException;

import com.onesat.api.admin.UsuarioDistribucionDTO;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;

public interface UsuarioDistribucionDao {

	public List<UsuarioDistribucionDTO> recuperarUsarios(UsuarioDistribucionDTO.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException;
}
