package com.onesat.api.admin.utilidades;

import java.awt.Color;

public class Colores {

	public static Color COLOR_ACTIVADO = Color.decode("#91c98d");
	public static Color COLOR_DESACTIVADO = Color.decode("#8c5d5d");
}
