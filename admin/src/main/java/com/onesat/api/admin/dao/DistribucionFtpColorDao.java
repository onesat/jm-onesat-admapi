package com.onesat.api.admin.dao;

import javax.xml.soap.SOAPException;

import com.onesat.api.admin.DistribucionFtpColorDTO;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;

public interface DistribucionFtpColorDao {

	public DistribucionFtpColorDTO recuperarDatos(DistribucionFtpColorDTO.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException;
}
