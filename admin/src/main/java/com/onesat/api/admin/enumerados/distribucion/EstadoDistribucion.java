package com.onesat.api.admin.enumerados.distribucion;

import java.awt.Color;

import com.onesat.api.admin.utilidades.Colores;

public enum EstadoDistribucion {

	ACTIVADA((short) 1, "ACTIVADA", Colores.COLOR_ACTIVADO),
	DESACTIVADA((short) 2, "DESACTIVADA", Colores.COLOR_DESACTIVADO);
	
	public short id;
	public String descripcion;
	
	public Color color;
	
	private EstadoDistribucion(short id, String descripcion, Color color) {
		
		this.id = id;
		this.descripcion = descripcion;
		
		this.color = color;
	}
}
