package com.onesat.api.admin.enumerados.usuario;

import com.onesat.api.admin.utilidades.Rutas;

public enum TipoUsuario {

	ADMINISTRADOR((short) 1, "ADMINISTRADOR"),
	RECEPCION((short) 2, "RECEPCION");
	
	public short id;
	public String descripcion;
	
	public String imagen;
	
	private TipoUsuario(short id, String descripcion) {
		
		this.id = id;
		this.descripcion = descripcion;
		
		this.imagen = Rutas.RUTA_IMAGENES + "usuario/" + descripcion.toLowerCase() + ".png";
	}
}
