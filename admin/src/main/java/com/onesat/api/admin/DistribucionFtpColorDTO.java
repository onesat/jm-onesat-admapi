package com.onesat.api.admin;

import com.onesat.api.admin.basico.Ftp;
import com.onesat.api.admin.basico.GamaColor;

public class DistribucionFtpColorDTO {

	private UsuarioDistribucionDTO distribucion;
	
	private Ftp ftp;
	private GamaColor color;
	
	public UsuarioDistribucionDTO getDistribucion() {
		return distribucion;
	}
	
	public void setDistribucion(UsuarioDistribucionDTO distribucion) {
		this.distribucion = distribucion;
	}

	public Ftp getFtp() {
		return ftp;
	}

	public void setFtp(Ftp ftp) {
		this.ftp = ftp;
	}

	public GamaColor getColor() {
		return color;
	}

	public void setColor(GamaColor color) {
		this.color = color;
	}
	
	public static class Busqueda {
		
		private Integer idDistribucion;

		public Integer getIdDistribucion() {
			return idDistribucion;
		}

		public void setIdDistribucion(Integer idDistribucion) {
			this.idDistribucion = idDistribucion;
		}
	}
}
