package com.onesat.api.admin.facade.impl;

import java.util.List;

import javax.xml.soap.SOAPException;

import com.onesat.api.admin.UsuarioDistribucionDTO;
import com.onesat.api.admin.basico.Usuario;
import com.onesat.api.admin.dao.impl.UsuarioDaoImpl;
import com.onesat.api.admin.dao.impl.UsuarioDistribucionDaoImpl;
import com.onesat.api.admin.facade.FacadeService;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;

public class FacadeServiceImpl implements FacadeService {

	// USUARIO
	public List<Usuario> recuperarUsuarios(Usuario.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {

		return new UsuarioDaoImpl().recuperarUsuarios(busqueda);
	}
	
	public int crearUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {
		
		return new UsuarioDaoImpl().crearUsuario(usuario);
	}
	
	public boolean modificarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {

		return new UsuarioDaoImpl().modificarUsuario(usuario);
	}
	
	public boolean eliminarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {

		return new UsuarioDaoImpl().eliminarUsuario(usuario);
	}

	// USUARIO + DISTRIBUCION
	public List<UsuarioDistribucionDTO> recuperarUsuariosDistribucion(UsuarioDistribucionDTO.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {

		return new UsuarioDistribucionDaoImpl().recuperarUsarios(busqueda);
	}	
}
