package com.onesat.api.admin;

import com.onesat.api.admin.basico.Usuario;
import com.onesat.api.admin.enumerados.distribucion.EstadoDistribucion;
import com.onesat.api.admin.enumerados.usuario.TipoUsuario;
import com.repex.librerias.login.entidades.EstadoUsuario;

public class UsuarioDistribucionDTO {

	private Usuario usuario;
	
	private Integer idDistribucion;
	private String descripcionDistribucion;
	private EstadoDistribucion estadoDistribucion;
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getIdDistribucion() {
		return idDistribucion;
	}

	public void setIdDistribucion(Integer idDistribucion) {
		this.idDistribucion = idDistribucion;
	}

	public String getDescripcionDistribucion() {
		return descripcionDistribucion;
	}
	
	public void setDescripcionDistribucion(String descripcionDistribucion) {
		this.descripcionDistribucion = descripcionDistribucion;
	}
	
	public EstadoDistribucion getEstadoDistribucion() {
		return estadoDistribucion;
	}
	
	public void setEstadoDistribucion(EstadoDistribucion estadoDistribucion) {
		this.estadoDistribucion = estadoDistribucion;
	}
	
	public static class Busqueda {
		
		private String referencia;
		private String nombre;
		
		private EstadoUsuario estado;
		private TipoUsuario tipo;
		
		public String getReferencia() {
			return referencia;
		}
		
		public void setReferencia(String referencia) {
			this.referencia = referencia;
		}
		
		public String getNombre() {
			return nombre;
		}
		
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
		public EstadoUsuario getEstado() {
			return estado;
		}
		
		public void setEstado(EstadoUsuario estado) {
			this.estado = estado;
		}
		
		public TipoUsuario getTipo() {
			return tipo;
		}
		
		public void setTipo(TipoUsuario tipo) {
			this.tipo = tipo;
		}
	}
}
