package com.onesat.api.admin.facade;

import java.util.List;

import javax.xml.soap.SOAPException;

import com.onesat.api.admin.UsuarioDistribucionDTO;
import com.onesat.api.admin.basico.Usuario;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;

public interface FacadeService {
	
	// USUARIOS
	public List<Usuario> recuperarUsuarios(Usuario.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException;
	public int crearUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException;
	public boolean modificarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException;
	public boolean eliminarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException;
	
	public List<UsuarioDistribucionDTO> recuperarUsuariosDistribucion(UsuarioDistribucionDTO.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException;
}
