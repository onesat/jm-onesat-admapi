package com.onesat.api.admin.dao.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.onesat.api.admin.basico.Usuario;
import com.onesat.api.admin.dao.UsuarioDao;

import com.repex.librerias.criptografia.Criptografia;
import com.repex.librerias.login.entidades.EstadoUsuario;
import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseList;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;

public class UsuarioDaoImpl implements UsuarioDao {

	public List<Usuario> recuperarUsuarios(Usuario.Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {
		
		List<Usuario> usuarios = new LinkedList<Usuario>();
		
		// FUNCION
		String FUNCION = "recuperar_usuarios";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		if(busqueda != null) {
			
			if(busqueda.getId() > 0) {
				nodos.add(new SOAPEnvironment.Nodo("ID_USUARIO", "xsd:int", busqueda.getId()));				
			}
			if(busqueda.getNombre() != null && !busqueda.getNombre().equals("")) {
				nodos.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", busqueda.getNombre()));
			}
			if(busqueda.getEstado() != null) {
				nodos.add(new SOAPEnvironment.Nodo("ESTADO", "xsd:int", busqueda.getEstado().id));
			}
			if(busqueda.getTipo() != null) {
				nodos.add(new SOAPEnvironment.Nodo("TIPO", "xsd:int", busqueda.getTipo().id));
			}
		}
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("busqueda", "tns:BusquedaUsuario", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {
			
			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {
				
				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseList response = lastResponse.getMultiData();
			for(SOAPResponseMap map : response.getDatos()) {
				
				Usuario usuario = new Usuario();
				usuario.setId(Integer.parseInt(map.getAtributte("ID_USUARIO")));
				usuario.setNombre(map.getAtributte("NOMBRE"));
				usuario.setContrasena(Criptografia.getSingleton().decrypt(map.getAtributte("CONTRASENA")));
				usuario.setEstado(EstadoUsuario.values()[Integer.parseInt(map.getAtributte("ESTADO")) - 1]);
				
				usuarios.add(usuario);
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
			
		} catch (SAXException e) {
			
			e.printStackTrace();
		}
				
		return usuarios;
	}

	public int crearUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {
		
		// FUNCION
		String FUNCION = "crear_usuario";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("REFERENCIA", "xsd:string", usuario.getReferencia()));
		nodos.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", usuario.getNombre()));
		nodos.add(new SOAPEnvironment.Nodo("CONTRASENA", "xsd:string", Criptografia.getSingleton().encrypt(usuario.getContrasena())));
		nodos.add(new SOAPEnvironment.Nodo("ESTADO", "xsd:int", usuario.getEstado().id));
		nodos.add(new SOAPEnvironment.Nodo("TIPO", "xsd:int", usuario.getTipo().id));
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("usuario", "tns:Usuario", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {

			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {

				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseMap response = lastResponse.getData();
			return Integer.parseInt(response.getAtributte("ID_USUARIO"));

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ParserConfigurationException e) {

			e.printStackTrace();

		} catch (SAXException e) {

			e.printStackTrace();
		}
		
		return 0;
	}

	public boolean modificarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {
		
		// FUNCION
		String FUNCION = "modificar_usuario";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("ID_USUARIO", "xsd:int", usuario.getId()));
		nodos.add(new SOAPEnvironment.Nodo("REFERENCIA", "xsd:string", usuario.getReferencia()));
		nodos.add(new SOAPEnvironment.Nodo("NOMBRE", "xsd:string", usuario.getNombre()));
		nodos.add(new SOAPEnvironment.Nodo("CONTRASENA", "xsd:string", Criptografia.getSingleton().encrypt(usuario.getContrasena())));
		nodos.add(new SOAPEnvironment.Nodo("ESTADO", "xsd:int", usuario.getEstado().id));
		nodos.add(new SOAPEnvironment.Nodo("TIPO", "xsd:int", usuario.getTipo().id));
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("usuario", "tns:Usuario", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {

			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {

				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseMap response = lastResponse.getData();
			return Boolean.parseBoolean(response.getAtributte("BOOLEAN"));

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ParserConfigurationException e) {

			e.printStackTrace();

		} catch (SAXException e) {

			e.printStackTrace();
		}
		
		return false;
	}

	public boolean eliminarUsuario(Usuario usuario) throws SOAPException, RequiredAuthenticationException {
		
		// FUNCION
		String FUNCION = "eliminar_usuario";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		nodos.add(new SOAPEnvironment.Nodo("ID_USUARIO", "xsd:int", usuario.getId()));
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, nodos);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {

			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {

				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseMap response = lastResponse.getData();
			return Boolean.parseBoolean(response.getAtributte("BOOLEAN"));

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ParserConfigurationException e) {

			e.printStackTrace();

		} catch (SAXException e) {

			e.printStackTrace();
		}
		
		return false;
	}

}
