package com.onesat.api.admin.basico;

public class Ftp {

	private int id;
	
	private String descripcion;
	
	private String usuario;
	private String contrasena;
	private String servidor;
	private String carpeta;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = (descripcion == null) ? "" : descripcion;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = (usuario == null) ? "" : usuario;
	}
	
	public String getContrasena() {
		return contrasena;
	}
	
	public void setContrasena(String contrasena) {
		this.contrasena = (contrasena == null) ? "" : contrasena;
	}
	
	public String getServidor() {
		return servidor;
	}
	
	public void setServidor(String servidor) {
		this.servidor = (servidor == null) ? "" : servidor;
	}
	
	public String getCarpeta() {
		return carpeta;
	}
	
	public void setCarpeta(String carpeta) {
		this.carpeta = (carpeta == null) ? "" : carpeta;
	}
}
