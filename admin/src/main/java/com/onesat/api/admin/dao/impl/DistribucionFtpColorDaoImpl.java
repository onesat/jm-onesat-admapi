package com.onesat.api.admin.dao.impl;

import java.awt.Color;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.xml.sax.SAXException;

import com.onesat.api.admin.DistribucionFtpColorDTO;
import com.onesat.api.admin.DistribucionFtpColorDTO.Busqueda;
import com.onesat.api.admin.basico.Ftp;
import com.onesat.api.admin.basico.GamaColor;
import com.onesat.api.admin.dao.DistribucionFtpColorDao;
import com.repex.librerias.criptografia.Criptografia;
import com.repex.librerias.soap.SOAPControler;
import com.repex.librerias.soap.SOAPEnvironment;
import com.repex.librerias.soap.exception.RequiredAuthenticationException;
import com.repex.librerias.soap.utilidades.SOAPResponseEnvelopment;
import com.repex.librerias.soap.utilidades.SOAPResponseMap;

public class DistribucionFtpColorDaoImpl implements DistribucionFtpColorDao {

	public DistribucionFtpColorDTO recuperarDatos(Busqueda busqueda) throws SOAPException, RequiredAuthenticationException {
		
		// FUNCION
		String FUNCION = "recuperar_ftp_color";

		List<SOAPEnvironment.Header> funcionHeaders = new LinkedList<SOAPEnvironment.Header>();
		funcionHeaders.add(new SOAPEnvironment.Header("SOAP-ENV:encodingStyle", "http://schemas.xmlsoap.org/soap/encoding/"));

		SOAPEnvironment.Funcion funcion = new SOAPEnvironment.Funcion(FUNCION, "ns1", funcionHeaders);

		// NODOS
		List<SOAPEnvironment.Nodo> nodos = new LinkedList<SOAPEnvironment.Nodo>();
		if(busqueda != null) {
			
			if(busqueda.getIdDistribucion() != null && busqueda.getIdDistribucion() > 0) {
				nodos.add(new SOAPEnvironment.Nodo("ID_DISTRIBUCION", "xsd:int", busqueda.getIdDistribucion()));				
			}
		}
		
		// PARAMETROS
		List<SOAPEnvironment.Parametro> parametros = new LinkedList<SOAPEnvironment.Parametro>();
		parametros.add(new SOAPEnvironment.Parametro("busqueda", "tns:BusquedaDistribucionFtpColor", nodos));

		SOAPEnvironment soapEnvironment = new SOAPEnvironment(funcion, parametros, null);

		// PARSE RESPONSE
		SOAPMessage soapResponse = SOAPControler.getSingleton().callSoapWebService(FUNCION, soapEnvironment);

		try {
			
			SOAPResponseEnvelopment lastResponse = new SOAPResponseEnvelopment(soapResponse);
			if(lastResponse.isError() != null) {
				
				throw new SOAPException(lastResponse.getError());
			}

			SOAPResponseMap map = lastResponse.getData();
			if(map != null) {
				
				DistribucionFtpColorDTO distribucionFtpColor = new DistribucionFtpColorDTO();

				try {
					
					Ftp ftp = new Ftp();
					ftp.setId(Integer.parseInt(map.getAtributte("ID_FTP")));
					ftp.setDescripcion(map.getAtributte("DESCRIPCION_FTP"));
					ftp.setUsuario(map.getAtributte("USUARIO_FTP"));
					ftp.setServidor(map.getAtributte("SERVIDOR_FTP"));
					ftp.setCarpeta(map.getAtributte("CARPETA_FTP"));

					String contrasena = map.getAtributte("CONTRASENA_FTP");
					if(contrasena != null && !contrasena.equals(""))
						ftp.setContrasena(Criptografia.getSingleton().decrypt(contrasena));
					
					distribucionFtpColor.setFtp(ftp);
					
				} catch (Exception e) {}

				try {
					
					GamaColor color = new GamaColor();
					color.setId(Integer.parseInt(map.getAtributte("ID_COLOR")));
					color.setDescripcion(map.getAtributte("DESCRIPCION_COLOR"));
					color.setCabecera(decode(map.getAtributte("CABECERA_COLOR")));
					color.setElemento(decode(map.getAtributte("ELEMENTO_COLOR")));
					color.setFondo(decode(map.getAtributte("FONDO_COLOR")));
					color.setTitulo(decode(map.getAtributte("TITULO_COLOR")));
					color.setTexto(decode(map.getAtributte("TEXTO_COLOR")));

					distribucionFtpColor.setColor(color);
					
				} catch (Exception e) {}
				
				return distribucionFtpColor;
			}
			
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
			
		} catch (SAXException e) {
			
			e.printStackTrace();
		}
				
		return null;
	}
	
	private Color decode(String color) {
		
		try {
			
			return Color.decode(color);
			
		} catch (Exception e) {}
		
		return null;
	}
}
